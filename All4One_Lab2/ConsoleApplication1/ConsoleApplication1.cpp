//Lab Exercise 2 - All4One
#include <iostream>

//Complete
void convertCF()
{
    double temperature;
    double farenheit;
    //Get input from user
    std::cout << "Please input temperature in Celcius: ";
    std::cin >> temperature;

    //Change from celcius to Fahrenheit
    farenheit = (temperature * 9 / 5) + 32;
    std::cout << "The temperature in Fahrenheit is " << farenheit << std::endl << std::endl;
}

//Complete
void convertFC()
{
    double temperature;
    double celsius;
    //Get input from user
    std::cout << "Please input a value in Fahrenheit: ";
    std::cin >> temperature;

    //Change from celcius to Fahrenheit
    celsius = (temperature - 32) * 5/9;
    std::cout << "The temperature in Celsius is " << celsius << std::endl << std::endl;
}

//Complete
void calculateCircum()
{
    double radius, circumference;

    std::cout << "Enter radius (in cm): ";
    std::cin >> radius;

    if (radius > 1)
    {
        circumference = 2 * 3.142 * radius;
        std::cout << "The circumference is " << circumference << "cm" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Invalid input. Radius should be > 1" << std::endl << std::endl;
    }
}

//Complete
void calculateCirArea()
{
    double radius, area;

    std::cout << "Enter radius (in cm): ";
    std::cin >> radius;

    if (radius > 1)
    {
        area = 3.142 * radius * radius;
        std::cout << "The area is " << area << "cm squared" << std::endl << std::endl;
    }
    else
    {
        std::cout << "Invalid input. Radius should be > 1" << std::endl << std::endl;
    }
}

//Complete
void calculateRecArea()
{
    double length;
    double width;
    double AreaRec;

    std::cout << "Enter a length(in cm):";
    std::cin >> length;
    
    if (length < 1)
    {
        std::cout << "Invalid Input! Please Enter Value more than 1." << std::endl;
    }
    std::cout << "Enter a width(in cm):";
    std::cin >> width;

    if (width < 1)
    {
        std::cout << "Invalid Input! Please Enter Value more than 1." << std::endl;
    }

    AreaRec = length * width;
    std::cout << "Area of Rectangle :" << AreaRec << " cm" << std::endl;

}

//Complete
void calculateTriArea()
{
    double a;
    double b;
    double c;
    double s;
    double AreaTri;

    std::cout << "Enter a Length A (in cm):";
    std::cin >> a;
    if (a < 1)
    {
        std::cout << "Invalid Input! Please Enter Value more than 1." << std::endl;
    }

    std::cout << "Enter a Length B (in cm):";
    std::cin >> b;
    if (b < 1)
    {
        std::cout << "Invalid Input! Please Enter Value more than 1." << std::endl;
    }

    std::cout << "Enter a Length C (in cm):";
    std::cin >> c;
    if (c < 1)
    {
        std::cout << "Invalid Input! Please Enter Value more than 1." << std::endl;
    }

    s = (a + b + c)/2;
    AreaTri = sqrt(s*(s-a)*(s-b)*(s-c));
    std::cout << "Area of Triangle :" << AreaTri << " cm" << std::endl;
    
}

//Complete
void calculateCylVol()
{
    double input;
    double height;
    double volume;
    
    std::cout << "Input Radius: ";
    
    std::cin >> input;
    std::cout << "Input Height: ";

    std::cin >> height;
    if (input >= 1 && height >= 1)
    {
        volume = 3.142 * (input * input) * height;
        std::cout << "Volume = " << volume << std::endl;
    }
    else
    {
        std::cout << "Invalid Input" << std::endl;
    }
}

//Complete
void calculateConeVol()
{
    double radius;
    double height;
    double volume;

    std::cout << "Input Radius: ";

    std::cin >> radius;
    std::cout << "Input Height: ";

    std::cin >> height;
    if (radius >= 1 && height >= 1)
    {
        volume = 3.142 * (radius * radius) * (height / 3);
        std::cout << "Volume = " << volume << std::endl;

    }
    else
    {
        std::cout << "Invalid Input" << std::endl ;
    }   
}

int main()
{
    bool repeat = true;

    while (repeat)
    {
        int choice;
        std::cout << "Menu" << std::endl
        << "1. Convert from Celsius to Fahrenheit" << std::endl
        << "2. Convert from Fahrenheit to Celsius" << std::endl
        << "3. Calculate Circumference of a Circle"<< std::endl
        << "4. Calculate Area of a Circle" << std::endl
        << "5. Calculate Area of Rectangle" << std::endl
        << "6. Calculate Area of Triangle (Heron's Formula)" << std::endl
        << "7. Calculate Volume of Cylinder" << std::endl
        << "8. Calculate Volume of Cone" << std::endl
        << "9. Quit program" << std::endl
        << "Enter your choice: ";
        std::cin >> choice;

        std::cout << std::endl;
        switch (choice)
        {
            case 1: convertCF();
                break;
            case 2: convertFC();
                break;
            case 3: calculateCircum();
                break;
            case 4: calculateCirArea();
                break;
            case 5: calculateRecArea();
                break;
            case 6: calculateTriArea();
                break;
            case 7: calculateCylVol();
                break;
            case 8: calculateConeVol();
                break;
            case 9: std::cout << "Goodbye!" << std::endl;
                repeat = false;
                break;
            default: std::cout << "Invalid choice" << std::endl;
                break;
        }
    }
}

